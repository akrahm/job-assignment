import React from 'react';
import JobBrief from './JobBrief';
import JobCard from './JobCard';


const App = () =>{
  return (
    <div className= "ui portal">
      <div className="details content">
      <div>
          <h1>Job search Portal</h1>
          <br/>
              <div className="ui job image">
                  <img src="https://picsum.photos/id/180/500/200" class="company-banner"/>
                  
              </div>
          <br/>
        <div className="Search-panel" style={{marginTop:"10px", marginLeft:"10px"}}>
           <sapan>
               <label class = "label" for= "name">What:</label>
               <input id= "name" type = "text" placeholder="keywords, Job title, company" style={{paddingRight:"2px" , marginRight:"5px"}}/>
          </sapan> 
          <sapan>
               <label class = "label" for= "name">Where:</label>
               <input id= "name" type = "text" placeholder="Cityname" style={{ marginRight:"5px"}}/>
          </sapan> 
          <div className="ui basic green button">Find Jobs</div>
          
        </div>
        <br />
        <div>
        <JobCard>
        <JobBrief 
      name= "Full-stack developer"
      city="delhi"
      country="India"
      description= "Experience and understanding of MERN stack (Mongo DB, Express.js, React Js, Node.js) or similar frameworks like AngularJS, Vue Js, D3 Js, etc. will be appreciated."
      salary= "65000"
      logo= "https://picsum.photos/id/180/200/100"
    />
        </JobCard>
        

        <JobCard>
        <JobBrief 
      name= "HR"
      city="Chandigarh"
      country="India"
      description= "HRIS Administrator responsibilities include managing our internal databases, keeping employee records in digital formats and educating users on how to use our HR systems. To be successful in this role, you should have experience with database administration and Human Resources Management software, like payroll or applicant tracking systems."
      salary= "25000"
      logo= "https://picsum.photos/id/180/200/100"
    />
        </JobCard>
        
        <JobCard>
        <JobBrief 
      name= "Back-end developer"
      city="Pune"
      country="India"
      description= "We are looking for an excellent experienced person in Backend Developer field. Be a part of a vibrant, rapidly growing tech enterprise with a great working environment. As an Backend developer you will be responsible for the server side of our web applications and you will work closely with our engineers to ensure the system consistency and improve your experience."
      salary= "40000"
      logo= "https://picsum.photos/id/180/200/100"
    />
        </JobCard>

        <JobCard>
        <JobBrief 
      name= "Front-end developer"
      city="Pune"
      country="India"
      description= "We are looking for a talented User Experience Designer to create amazing user experiences. The ideal candidate should have an eye for clean and artful design, possess superior UI skills and be able to translate high-level requirements into interaction flows and artifacts, and transform them into beautiful, intuitive, and functional user interfaces. Expert understanding of contemporary user-centered design methodologies for web & mobile is a must."
      salary= "40000"
      logo= "https://picsum.photos/id/180/200/100"
    />
        </JobCard>

        <JobCard>
        <JobBrief 
      name= "SEO"
      city="Bangalore"
      country="India"
      description= "Experienced in Google ad-words, SEO, SMO, SEM (on page & off page) PPC, Digital marketing campaigns, Marketing database, social media and display advertising campaigns."
      salary= "15000"
      logo= "https://picsum.photos/id/180/200/100"
    />
        </JobCard>
        </div>
      </div>
     </div>
    </div>
  );

}


export default App;
