import React from 'react';


const JobBrief = (props) => {
    return (
       <div className="jobs">
           <div>
               <h4>{props.name}</h4>
               <h5>{props.city}</h5>
               <img src= {props.logo}  />
               <p>Description:{props.description}</p>
               <div>Salary: {props.salary}</div>
           </div>
       </div>
    );
}


export default JobBrief;
