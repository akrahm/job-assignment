import React from 'react';


const JobCard = (props) => {
    return (
        <div className="ui card" style={{marginLeft:"20px"}}>
            <div className="context">{props.children}</div>
            <div className="extra content">
              <div className="ui two buttons">
                  <div className="ui basic green button">Apply</div>
                  <div className="ui basic red button">Not Interested</div>
              </div>
            </div>
        </div>
    );
}

export default JobCard;
